import React from 'react'

export default function FightOtherPlayer() {
    return (
        <div>
            <div class="container">
                <div class="wrap">
                    <p>player 1</p>
                    <div class="box rock" id="rock"><img src="assets/batu.png" alt="rock"/></div>
                    <div class="box paper" id="paper"><img src="assets/kertas.png" alt="paper"/></div>
                    <div class="box scissor" id="scissor"><img src="assets/gunting.png" alt="scissor"/></div>
                </div>
                <div class="wrap-center">
                    <div>
                        <div class="versus" id="versus">vs</div>
                        <div class="result">
                            <div class="win" id="playerWin">player 1 win</div>
                            <div class="lose" id="computerWin">com<br/>win</div>
                            <div class="draw" id="draw">draw</div>
                        </div>
                    </div>
                    <div class="wrap-button">
                        <button class="btn" id="reset">
                            <img src="assets/refresh.png" alt="reset"/>
                        </button>
                    </div>
                </div>
                <div class="wrap">
                    <p>com</p>
                    <div class="box rock" id="rockCom"><img src="assets/batu.png" alt="rock" /></div>
                    <div class="box paper" id="paperCom"><img src="assets/kertas.png" alt="paper" /></div>
                    <div class="box scissor" id="scissorCom"><img src="assets/gunting.png" alt="scissor" /></div>
                </div>
            </div>
        </div >
    )
}
