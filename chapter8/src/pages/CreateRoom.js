import React, { useState } from 'react'
import Input from '../components/input';
import rock from '../assets/batu.png';
import paper from '../assets/kertas.png';
import scissors from '../assets/gunting.png';
import Button from '../components/button';
import { Link } from 'react-router-dom';

export default function CreateRoom() {
    const [roomName, setRoomName] = useState();

    return (
        <div className='create-new-room'>
            <div className='room-name'>
                <Input
                    fontSize="1.5rem"
                    padding="0.5rem 1.5rem"
                    type="text"
                    placeholder="input the game room name" 
                    handleClick={(event) => {
                        setRoomName(event.target.value);
                    }}/>
            </div>
            <div>
                <div className='your-choice'>your choice</div>
                <div className='choices'>
                    <div className="box rock" id="rock">
                        <img src={rock} alt="rock" />
                    </div>
                    <div className="box paper" id="paper">
                        <img src={paper} alt="paper" />
                    </div>
                    <div className="box scissor" id="scissor">
                        <img src={scissors} alt="scissor" />
                    </div>
                </div>
                <div className='btn-save'>
                    <Link>
                        <Button 
                        title="save" 
                        handleClick={() => {
                            console.log(`room name: ${roomName}`)
                        }}/>
                    </Link>
                </div>
            </div>
        </div>
    )
}
