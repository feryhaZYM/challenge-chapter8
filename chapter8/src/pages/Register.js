import Button from "../components/button";
import Input from "../components/input";
import SocialMedia from "../components/social-media";
import BtnTransparent from "../components/btnTransparent";
import { Link } from "react-router-dom";
import { useState } from "react";

function Login() {
    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    return (
        <div className="container-authentication">
            <div className="form-container">
                <form className="auth-form">
                    <h2 className="title">Sign Up</h2>
                    <Input
                        type="text"
                        placeholder="username"
                        handleChange={(event) => {
                            setUsername(event.target.value);
                        }} />
                    <Input
                        type="email"
                        placeholder="email"
                        handleChange={(event) => {
                            setEmail(event.target.value);
                        }} />
                    <Input
                        type="password"
                        placeholder="password"
                        handleChange={(event) => {
                            setPassword(event.target.value);
                        }} />
                    <Button
                        margin="5px"
                        title="Register"
                        handleClick={() => {
                            console.log(`username: ${username}`);
                            console.log(`email: ${email}`);
                            console.log(`password: ${password}`);
                            console.log(`send to backend`);
                        }} />
                    <SocialMedia text="Sign Up" />
                </form>
            </div>
            <div className="panel-container">
                <h3>one of us?</h3>
                <Link to={'/login'}>
                    <BtnTransparent title="sign up" />
                </Link>
            </div>
        </div>

    );
};

export default Login;