import React from 'react'
import { Link, Outlet } from 'react-router-dom'

export default function Layout() {
    return (
        <div>
            <div className='navbar'>
                <div className='dashboard-nav'>
                    <Link to={"dashboard"} style={{textDecoration: "none"}}>
                        <div>dashboard</div>
                    </Link>
                </div>
                <div className='authentication-nav'>
                    <Link to={"login"} style={{textDecoration: "none"}}>
                        <div className='login'>login</div>
                    </Link>
                    <Link to={"register"} style={{textDecoration: "none"}}>
                        <div className='register'>register</div>
                    </Link>
                </div>
            </div>
            <div>
                <Outlet />
            </div>
        </div>
    )
}
