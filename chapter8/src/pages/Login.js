import Button from "../components/button";
import Input from "../components/input";
import SocialMedia from "../components/social-media";
import { Link } from "react-router-dom";
import BtnTransparent from "../components/btnTransparent";
import { useState } from "react";

function Login() {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    return (
        <div className="container-authentication">
            <div className="form-container">
                <form className="auth-form">
                    <h2 className="title" style={{ marginBottom: "60px" }}>Sign In</h2>
                    <Input
                        type="text"
                        placeholder="username"
                        handleChange={(event) => {
                            setUsername(event.target.value);
                        }} />
                    <Input
                        type="password"
                        placeholder="password"
                        handleChange={(event) => {
                            setPassword(event.target.value);
                        }} />
                    <Button margin="20px" title="login" 
                    handleClick={() => {
                        console.log(`username: ${username}`);
                        console.log(`password: ${password}`);
                        console.log(`send to backend`);
                    }}/>
                    <SocialMedia marginLogin="50px" text="Sign In" />
                </form>
            </div>
            <div className="panel-container">
                <h3>new here?</h3>
                <Link to={'/register'}>
                    <BtnTransparent title="sign up" />
                </Link>
            </div>
        </div>

    );
};

export default Login;