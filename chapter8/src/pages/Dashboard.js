import React from 'react'
import BoxFight from '../components/box-fight';
import Button from '../components/button';
import { Link } from 'react-router-dom';
import PrevNext from '../components/prevNext';
import Biodata from '../components/biodata';
import ProfilePicture from '../components/profile-picture';

export default function Dashboard() {
    return (
        <div className='dashboard'>
            <div className='game'>
                <div className='box-vscom'>
                    <BoxFight value="playing with com" />
                    <div className='horizontal-line'></div>
                </div>
                <div className='create-room'>
                    <h3>create new room</h3>
                    <Link to={"create-room"}>
                        <Button title="Create" />
                    </Link>

                </div>
                <h3 className='title-room'>available room</h3>
                <div className='rooms'>
                    <div className='rooms-row'>
                        <BoxFight value="Room Sirkel" color="white" textTransform="none" winner="winner : santoso" />
                        <BoxFight value="Omdamdink" color="white" textTransform="none" winner="winner : mhanx oleh" />
                        <BoxFight value="Zeeber" color="white" textTransform="none" winner="winner : chachink" />
                        <BoxFight value="PeBeh" color="white" textTransform="none" winner="winner : Jinx|pro*Amanda" />
                    </div>
                    <div className='rooms-row'>
                        <BoxFight value="Ainxx" color="white" textTransform="none" winner="winner : ..." />
                        <BoxFight value="Zeeeber" color="white" textTransform="none" winner="winner : ..." />
                        <BoxFight value="Ea" color="white" textTransform="none" winner="winner : ..." />
                        <BoxFight value="Mhanx" color="white" textTransform="none" winner="winner : ..." />
                    </div>
                    <div className='rooms-row'>
                        <BoxFight value="VVad00 Mhanx" color="white" textTransform="none" winner="winner : ..." />
                        <BoxFight value="Chachinx jiber" color="white" textTransform="none" winner="winner : ..." />
                        <BoxFight value="Room" color="white" textTransform="none" winner="winner : ..." />
                        <BoxFight value="Gaminxxx" color="white" textTransform="none" winner="winner : ..." />
                    </div>
                </div>
                <div>
                    <PrevNext />
                </div>
            </div>
            <div className='biodata'>
                <div>
                    <ProfilePicture />
                </div>
                <div>
                    <Biodata />
                </div>
                <Link>
                    <Button title="game history" />
                </Link>
            </div>
        </div>
    )
}
