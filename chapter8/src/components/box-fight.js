import React from 'react'
import "./box-fight.css"

export default function BoxFight(props) {
    let boxValue = props.value;
    let winner = props.winner;

    return (
        <div className='box-fight' style={{backgroundColor: props.color,}}>
            <h2 style={{
                    textTransform: props.textTransform,
                    color: props.textColor,
                }}>{boxValue}</h2>
            <p>{winner}</p>
        </div>
    )
}
