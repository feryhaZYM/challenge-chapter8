import React from 'react'
import "./button.css"

export default function BtnTransparent(props) {
    let btnTitle = props.title ?? "submit"
    return (
        <div>
            <button className='btn transparent'>
                {btnTitle}
            </button>
        </div>
    )
}
