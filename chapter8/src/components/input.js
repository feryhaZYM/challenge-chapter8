import "./input.css";

function Input(props) {
    let inputType = props.type ?? "text";
    let inputPlaceholder = props.placeholder ?? "enter text here";

    return (
        <div className="input-field" style={{padding: props.padding}}>
            <input style={{fontSize: props.fontSize}} type={inputType} placeholder={inputPlaceholder} 
            onChange={props.handleChange}/>
        </div>
    )
}

export default Input;