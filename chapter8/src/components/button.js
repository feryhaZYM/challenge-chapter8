import "./button.css";

function Button(props) {
    let buttonTitle = props.title ?? "submit";

    return (
        <button 
        className="button"
        style={{margin: props.margin}}
        onClick={props.handleClick}
        >
            {buttonTitle}
        </button>
    );
};

export default Button;