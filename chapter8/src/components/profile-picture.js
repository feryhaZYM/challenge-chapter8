import React from 'react'
import picture from '../assets/pictureDefault.png'

export default function ProfilePicture() {
  return (
    <div>
        <img src={picture} alt="saiyan" width="250rem" height="250rem" style={{borderRadius: "50%", borderStyle: 'solid', backgroundColor: "#5995fd"}}/>
    </div>
  )
}
