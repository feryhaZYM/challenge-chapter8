import React from 'react'
import './prevNext.css'

export default function PrevNext() {
    return (
        <div className='prevnext'>
            <a href="/previous" className="previous">&laquo; Previous</a>
            <a href="/next" className="next">Next &raquo;</a>
        </div>
    )
}
