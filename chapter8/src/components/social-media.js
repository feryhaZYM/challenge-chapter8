import './social-media.css'

function SocialMedia(props) {
    let signText = props.text ?? "Submit";

    return (
        <div style={{marginTop: props.marginLogin}} className="container-social-media" >
            <p className="social-text">Or {signText} with social platforms</p>
            <div className="social-media">
                <a href="https://facebook.com" className="social-icon">
                    <i className="fa fa-facebook"></i>
                </a>
                <a href="google.com" className="social-icon">
                    <i className="fa fa-google"></i>
                </a>
            </div>
        </div>
    );
};

export default SocialMedia;