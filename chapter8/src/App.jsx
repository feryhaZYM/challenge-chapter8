import { Route, Routes } from "react-router-dom";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Dashboard from "./pages/Dashboard";
import CreateRoom from "./pages/CreateRoom";
import FightOtherPlayer from "./pages/FightOtherPlayer";
import "./App.css";
import Layout from "./pages/Layout";

function App() {
  return (
    <Routes>
      <Route path="" element={<Layout />}>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard">
          <Route path="" element={<Dashboard />} />
          <Route path="create-room" element={<CreateRoom />} />
        </Route>
      </Route>
      <Route path="/fight" element={<FightOtherPlayer />} />
      <Route path="*" element={<center>404page not found</center>} />
    </Routes>
  );
}

export default App;
